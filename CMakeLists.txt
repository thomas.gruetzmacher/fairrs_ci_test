
cmake_minimum_required(VERSION 3.8 FATAL_ERROR)
project(ci_test LANGUAGES CXX)
enable_language(CUDA)


if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
    message(STATUS "Setting build type to 'Release' as none was specified.")
    set(CMAKE_BUILD_TYPE Release CACHE STRING "Choose the type of build." FORCE)
endif()

set(target "ci_test_cuda")
add_executable("${target}")

# Force the language to be CUDA (works, but the main.cu approach is cleaner)
# set_source_files_properties(main.cpp.inc PROPERTIES LANGUAGE CUDA)
target_sources("${target}" PRIVATE main.cu)


#TODO maybe add the Ginkgo Architecture Selector in this project
target_compile_features("${target}"  PUBLIC cxx_std_14)
target_compile_options("${target}" PRIVATE
    $<$<COMPILE_LANGUAGE:CUDA>:--expt-relaxed-constexpr>
    )
