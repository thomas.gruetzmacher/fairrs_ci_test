#include <iostream>
#include <stdexcept>

#define CUDA_CALL(call)                                                  \
    do {                                                                 \
        auto err = call;                                                 \
        if (err != cudaSuccess) {                                        \
            std::cerr << "Cuda error in file " << __FILE__               \
                      << " L:" << __LINE__                               \
                      << "; Error: " << cudaGetErrorString(err) << '\n'; \
            throw std::runtime_error(cudaGetErrorString(err));           \
        }                                                                \
    } while (false)


// compile with: nvcc -std=c++14 --gpu-architecture sm_80 main.cu

__global__ void simple_kernel(int value)
{
    if (threadIdx.x == 0 && blockIdx.x == 0) {
        printf("Value inside the kernel: %d\n", value);
    }
}


int main()
{
    std::cout << "This is from the main function\n" << std::flush;
    simple_kernel<<<1, 1>>>(42);
    CUDA_CALL(cudaDeviceSynchronize());
    std::cout << "After the kernel synchronization\n";
}
